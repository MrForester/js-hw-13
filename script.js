const images = document.querySelectorAll(".image-to-show");
const imagesWrapper = document.querySelector(".images-wrapper");
const divForButton = document.createElement("div");
const buttonStop = document.createElement("button");
const buttonContinue = document.createElement("button");
imagesWrapper.after(divForButton);
divForButton.append(buttonStop);
divForButton.append(buttonContinue);
divForButton.classList.add("button-wrapper");
buttonStop.classList.add("button-style");
buttonContinue.classList.add("button-style");
buttonStop.innerHTML = "Призупинити";
buttonContinue.innerHTML = "Відновити показ";

let currentIndex = 0;
let startSlider = setInterval(testStop, 3000);

function stopSlider() {
  buttonStop.classList.add("button-color");
  setTimeout(() => {
    buttonStop.classList.remove("button-color");
  }, 1000);
  clearInterval(startSlider);
}

function continueSlider() {
  buttonContinue.classList.add("button-color");
  setTimeout(() => {
    buttonContinue.classList.remove("button-color");
  }, 1000);
  startSlider = setInterval(testStop, 3000);
}

function testStop() {
  images[currentIndex].classList.add("inactive-img");

  if (currentIndex < images.length - 1) {
    currentIndex++;
  } else {
    currentIndex = 0;
  }

  images[currentIndex].classList.remove("inactive-img");
}

buttonStop.addEventListener("click", stopSlider);
buttonContinue.addEventListener("click", continueSlider);
